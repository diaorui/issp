#include <math.h>
#include <memory.h>
#include <algorithm>
#include <assert.h>
#include "issp.h"
#include <stdio.h>
using namespace std;

const double TOL = 1e-8;

bool operator < (Interval i1, Interval i2) {
  return i1.b - i1.a < i2.b - i2.a;
}

void update_delta(const double T, const double v, const double span, double *tmp1, const int id, int *last1) {
  assert(v > 0);
  if (v <= T) {
    int k = (int)(ceil(v / span) + 0.5) - 1;
    if (tmp1[k * 2] == 0 || v < tmp1[k * 2]) {
      tmp1[k * 2] = v;
      last1[k * 2] = id;
    }
    if (tmp1[k * 2 + 1] == 0 || v > tmp1[k * 2 + 1]) {
      tmp1[k * 2 + 1] = v;
      last1[k * 2 + 1] = id;
    }
  }
}

void relaxed_dp(const int n, Interval* interval, const double T, const double span, double *delta1, double *tmp1, int *last1) {

  int m = (int)(ceil(T / span) + 0.5);

  memset(delta1, 0, sizeof(delta1[0]) * 2 * m);

  if (T <= span || n == 0) return;

  memset(tmp1, 0, sizeof(tmp1[0]) * 2 * m);

  for (int i = 0; i < n; i++) {
    update_delta(T, interval[i].a, span, tmp1, i * 2, last1);
    update_delta(T, interval[i].b, span, tmp1, i * 2 + 1, last1);
    for (int j = 0; j < 2 * m; j++) {
      if (delta1[j] > 0) {
        update_delta(T, delta1[j] + interval[i].a, span, tmp1, i * 2, last1);
        update_delta(T, delta1[j] + interval[i].b, span, tmp1, i * 2 + 1, last1);
      }
    }
    memcpy(delta1, tmp1, sizeof(tmp1[0]) * 2 * m);
  }
}

void backtrack(const int n, Interval *interval, const double T, const double span, double *delta1, int *last1, double &y, int &n_new) {
  int m = (int)(ceil(T / span) + 0.5);
  double u = 0;
  int pos = -1;

  y = 0;
  n_new = n;

  for (int i = 0; i < 2 * m; i++) {
    if (delta1[i] > 0 && delta1[i] <= T) {
      u = delta1[i];
      pos = last1[i];
    }
  }
  assert(u != 0);
  assert(pos >= 0);

  while (true) {
    if (pos % 2) {
      interval[pos / 2].x = interval[pos / 2].b;
    }
    else {
      interval[pos / 2].x = interval[pos / 2].a;
    }
    y += interval[pos / 2].x;
    u -= interval[pos / 2].x;
    n_new = pos / 2;

    if (u > 0) {
      int j = (int)(ceil(u / span) + 0.5) - 1;
      if (delta1[j * 2 + 1] + y <= T && last1[j * 2 + 1] < (pos - pos % 2)) {
        u = delta1[j * 2 + 1];
        pos = last1[j * 2 + 1];
      }
      else if (delta1[j * 2] + y >= T - span && last1[j * 2] < (pos - pos % 2)) {
        u = delta1[j * 2];
        pos = last1[j * 2];
      }
      else break;
    } else break;
  }
}

void divide_conquer(const int n, Interval *interval, const double T, const double span, double *delta1, double *delta2, int *last1, int *last2, double *tmp, double &T_approx) {
  if (n == 0) {
    T_approx = 0;
    return;
  }

  int m = (int)(ceil(T / span) + 0.5);

  relaxed_dp(n / 2, interval, T, span, delta1, tmp, last1);
  relaxed_dp(n - (n / 2), interval + (n / 2), T, span, delta2, tmp, last2);

  int pos1 = 0;
  int pos2 = 2 * m - 1;
  double best_delta = 0;
  double best_value = 0;

  for (int i = 0; i < 2 * m; i++) {
    assert(delta1[i] <= T);
    assert(delta2[i] <= T);
    if (delta1[i] > best_value) {
      best_value = delta1[i];
      best_delta = 0;
    }
    if (delta2[i] > best_value) {
      best_value = delta2[i];
      best_delta = delta2[i];
    }
  }

  while (pos1 < 2 * m) {
    if (delta1[pos1] == 0) {
      pos1++;
      continue;
    }
    while (pos2 >= 0) {
      if (delta2[pos2] == 0 || delta1[pos1] + delta2[pos2] > T) {
        pos2--;
        continue;
      }
      else {
        if (delta1[pos1] + delta2[pos2] > best_value) {
          best_value = delta1[pos1] + delta2[pos2];
          best_delta = delta2[pos2];
        }
        break;
      }
    }
    pos1++;
  }

  assert(best_value >= (T - span) * (1. - TOL) && best_value <= T * (1. + TOL));

  double y1_b = 0;
  double y1_dc = 0;
  double y2_b = 0;
  double y2_dc = 0;
  int n_new;

  if (T - best_delta > span * (1. + TOL)) {
    backtrack(n / 2, interval, T - best_delta, span, delta1, last1, y1_b, n_new);
  }
  if (T - best_delta - y1_b > span * (1. + TOL)) {
    divide_conquer(n_new, interval, T - best_delta - y1_b, span, delta1, delta2, last1, last2, tmp, y1_dc);
    relaxed_dp(n - (n / 2), interval + (n / 2), T - y1_b - y1_dc, span, delta2, tmp, last2);
  }
  if (T - y1_b - y1_dc > span * (1. + TOL)) {
    backtrack(n - (n / 2), interval + (n / 2), T - y1_b - y1_dc, span, delta2, last2, y2_b, n_new);
  }
  if (T - y1_b - y1_dc - y2_b > span * (1. + TOL)) {
    divide_conquer(n_new, interval + (n / 2), T - y1_b - y1_dc - y2_b, span, delta1, delta2, last1, last2, tmp, y2_dc);
  }

  T_approx = y1_b + y1_dc + y2_b + y2_dc;
}

void issp(const int n, Interval* interval, const double T, const double eps, double &T_approx) {
  assert(n > 0);
  assert(T > 0);
  assert(eps > 0 && eps < 1);

  double *delta1, *delta2;
  int *last1, *last2;
  double *tmp;

  double span = T * eps;  // size of sub-intervals.
  int m = (int)ceil((1 / eps) + 0.5);

  delta1 = (double *)malloc(sizeof(double) * m * 2);
  delta2 = (double *)malloc(sizeof(double) * m * 2);
  last1 = (int *)malloc(sizeof(int) * m * 2);
  last2 = (int *)malloc(sizeof(int) * m * 2);
  tmp = (double *)malloc(sizeof(double) * m * 2);
  sort(interval, interval + n);

  for (int i = 0; i < m * 2; i++) {
    delta1[i] = delta2[i] = 0;
  }
  memset(delta1, 0, sizeof(delta1[0]) * m * 2);
  memset(tmp, 0, sizeof(tmp[0]) * m * 2);
  for (int i = 0; i < n; i++) {
    interval[i].x = 0;
  }

  double best_value = 0;
  double best_delta = 0;
  int best_pos = -1;
  for (int i = 0; i < n; i++) {

    if (min(interval[i].b, T) > best_value && interval[i].a <= T * (1. + TOL)) {
      best_value = min(interval[i].b, T);
      best_pos = i;
      best_delta = 0;
    }

    for (int j = 0; j < 2 * m; j++) {
      if (delta1[j] > 0 && delta1[j] + interval[i].b > best_value && delta1[j] <= T - interval[i].a) {
        best_value = min(delta1[j] + interval[i].b, T);
        best_pos = i;
        best_delta = delta1[j];
      }
    }

    if (best_value >= T * (1. - TOL)) break;

    update_delta(T, interval[i].a, span, tmp, i * 2, last1);
    update_delta(T, interval[i].b, span, tmp, i * 2 + 1, last1);
    for (int j = 0; j < 2 * m; j++) {
      if (delta1[j] > 0) {
        update_delta(T, delta1[j] + interval[i].a, span, tmp, i * 2, last1);
        update_delta(T, delta1[j] + interval[i].b, span, tmp, i * 2 + 1, last1);
      }
    }

    memcpy(delta1, tmp, sizeof(delta1[0]) * m * 2);
  }

  if (best_value <= TOL) {
    T_approx = 0.;    
  }
  else {
    double T_new = min(best_delta + span, T - interval[best_pos].a);
    if (T_new > span * (1. + TOL)) {
      divide_conquer(best_pos, interval, T_new, span, delta1, delta2, last1, last2, tmp, T_approx);
      assert((T_new - span) * (1. - TOL) <= T_approx && T_approx <= T_new * (1. + TOL)) ;
    }
    else {
      T_approx = 0.;
    }

    interval[best_pos].x = min(interval[best_pos].b, T - T_approx);
    T_approx += interval[best_pos].x;
  }

  assert(T_approx >= (T - span) * (1. - TOL) || T_approx >= best_value * (1. - TOL));

  double T_validate = 0.;
  for (int i = 0; i < n; i++) {
    assert(interval[i].x >= interval[i].a * (1. - TOL) || interval[i].x <= TOL);
    assert(interval[i].x <= interval[i].b * (1. + TOL));
    T_validate += interval[i].x;
  }
  assert(fabs(T_validate - T_approx) <= T_approx * TOL);

  free(delta1);
  free(delta2);
  free(last1);
  free(last2);
  free(tmp);
  return;
}

