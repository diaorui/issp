#ifndef __ISSP_H__
#define __ISSP_H__

// The interval is defined as [a, b].
// x is the variable either equal to 0 or in interval [a, b].
// i is the index of the interval.
struct Interval {
  double a;  // the lower bound of the interval.
  double b;  // the upper bound of the interval.
  double x;  // variable x == 0 or x in [a, b].
  int i;  // the index of the interval.
};

// Main function for solving ISSP.
// maximize sum_i(x)
// subject to 
//   sum_i(x) <= T
//   x_i \in [a_i, b_i] or x_i = 0
// n - the number of intervals. (input)
// interval - a list of intervals. (input, output)
// T - the target value. (input)
// eps - the epsilon for FPTAS. (input)
// T_approx - the optimal objective value. (output)
void issp(const int n, Interval* interval, const double T, const double eps, double& T_approx);

#endif
