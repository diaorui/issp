#include <stdio.h>
#include <stdlib.h>
#include "issp.h"
#include <math.h>
#include <sys/time.h>
#include <algorithm>
using std::max;

Interval interval[100000];

// Test with Problem D.
void run1(const int n, const double K) {
  double T = 3e14;
  double T_approx;

  timeval start, end;
  const int N = 100;

  for (int M = 10; M <= 1000; M *= 10) {
    double e_sum = 0;
    double e_bad = 0;
    double t_sum = 0;
    double t_bad = 0;

    printf("Number of intervals: %d\n", n);
    printf("Tolerance: %.3f%%\n", 100. / M);
    printf("Sampling parameter: %.3f\n", K);
    for (int i = 0; i < N; i++) {
      for (int j = 0; j < n; j++) {
        double k = rand() * 1. / RAND_MAX * (K - 1.) + 1.;
        interval[j].b = (((unsigned long long)rand() + 1) * ((unsigned long long)rand() + 1) % 100000000000000ULL - k + 1)  + k;
        interval[j].a = (unsigned long long)(interval[j].b / k + 1e-6);
        interval[j].i = j;
      }

      gettimeofday( &start, NULL );
      issp(n, interval, T, 1. / M, T_approx);
      gettimeofday( &end, NULL );

      double e = (T - T_approx) / T;
      e_sum += e;
      e_bad = max(e_bad, e);
      double t = end.tv_sec - start.tv_sec + (end.tv_usec - start.tv_usec) / 1e6;
      t_sum += t;
      t_bad = max(t_bad, t);
    }
    printf("Average: error %.3f%%, time %.3f s\n", e_sum * 100. / N, t_sum / N);
    printf("Worst case: error %.3f%%, time %.3f s\n", e_bad * 100., t_bad);
    printf("======\n");
  }
}

// Test with Problem A. 
void run2(const int n) {
  int k = (int)(floor(log(n*1.) / log(2.)) + 0.5);
  double T = 0;
  double T_opt = 0;
  for (int i = 0; i < n; i++) {
    interval[i].b = pow(2., k + n + 1) + pow(2., k + i + 1) + 1.;
    interval[i].a = interval[i].b;
    interval[i].i = i;
    T += interval[i].b;
    if (n % 2 == 0 && i < n - 1 && i >= n - 1 - n / 2) {
      T_opt += interval[i].b;
    }
    else if (n % 2 == 1 && i >= n - n / 2) {
      T_opt += interval[i].b;
    }
  }
  T = floor(T / 2);
  double T_approx;

  timeval start, end;

  for (int M = 10; M <= 1000; M *= 10) {
    gettimeofday( &start, NULL );
    issp(n, interval, T, 1. / M, T_approx);
    gettimeofday( &end, NULL );

    printf("Number of intervals: %d\n", n);
    printf("Tolerance: %.3f%%\n", 100. / M);
    printf("Error : %.3f%%\n", (T_opt - T_approx) / T_opt * 100.);
    printf("Time : %.3f s\n", end.tv_sec - start.tv_sec + (end.tv_usec - start.tv_usec) / 1e6); 
    printf("======\n");
  }
}

// Test with Problem B.
void run3(const int n) {
  double T = (n - 1) / 2 * (double)n * (n + 1) + (double)n * (n - 1) / 2;
  double T_opt = 0;
  for (int i = 0; i < n; i++) {
    interval[i].b = (double)n * (n + 1) + i + 1;
    interval[i].a = interval[i].b;
    interval[i].i = i;
    if (i >= n - (n - 1) / 2) {
      T_opt += interval[i].b;
    }
  }
  double T_approx;

  timeval start, end;

  for (int M = 10; M <= 1000; M *= 10) {
    gettimeofday( &start, NULL );
    issp(n, interval, T, 1. / M, T_approx);
    gettimeofday( &end, NULL );

    printf("Number of intervals: %d\n", n);
    printf("Tolerance: %.3f%%\n", 100. / M);
    printf("Error : %.3f%%\n", (T_opt - T_approx) / T_opt * 100.);
    printf("Time : %.3f s\n", end.tv_sec - start.tv_sec + (end.tv_usec - start.tv_usec) / 1e6); 
    printf("======\n");
  }
}

int main() {
  printf("Test results for Problem A\n======\n");
  run2(10);
  run2(15);
  run2(20);
  run2(25);
  run2(30);
  run2(35);

  printf("Test results for Problem B\n======\n");
  run3(10);
  run3(50);
  run3(100);
  run3(500);
  run3(1000);
  run3(5000);
  run3(10000);

  printf("Test results for Problem D\n======\n");
  int N[] = {1000, 5000, 10000, 50000, 100000};
  double K[] = {1.5, 1.3, 1.1}; 
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 3; j++) {
      run1(N[i], K[j]); 
    }
  } 

  return 0;
}
