# README #

* This repository is for solving Interval Subset Sum Problem (ISSP). Please click [HERE](src) to view source code.
* Contact: diaorui1987@gmail.com

# File description #
* main.cpp - The file for testing different problems.
* issp.h - The declaration of main ISSP solver interface.
* issp.cpp - The implementation of ISSP solver.